from pandas.io.parsers import read_csv
from joblib import load


def read_model():
    with open('predictor_model.joblib', 'rb') as model_file:
        regressor = load(model_file)
    return regressor


def read_encoders():
    with open('venue_encoder.joblib', 'rb') as venue_encoder_file:
        venue_encoder = load(venue_encoder_file)
    with open('team_encoder.joblib', 'rb') as team_encoder_file:
        team_encoder = load(team_encoder_file)
    return venue_encoder, team_encoder


def parse_input(input_file, venue_encoder, team_encoder):
    test_data = read_csv(input_file)
    test_data['venue'] = venue_encoder.transform(test_data['venue'])
    test_data['batting_team'] = team_encoder.transform(test_data['batting_team'])
    test_data['bowling_team'] = team_encoder.transform(test_data['bowling_team'])
    test_data = test_data[['venue', 'innings', 'batting_team', 'bowling_team']]

    return test_data
