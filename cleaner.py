from pandas.io.parsers import read_csv


def clean_data(unclean_dataset):
    unclean_dataset = unclean_dataset[["match_id", "venue", "innings", "ball", "batting_team", "bowling_team", "striker", "non_striker", "bowler", "runs_off_bat", "extras", "wides", "noballs", "byes", "legbyes", "penalty"]]
    unclean_dataset['total_runs'] = unclean_dataset['runs_off_bat'] + unclean_dataset['extras']
    unclean_dataset = unclean_dataset.drop(columns=['runs_off_bat', 'extras'])
    unclean_dataset = unclean_dataset[unclean_dataset['ball'] <= 5.6]
    unclean_dataset = unclean_dataset[unclean_dataset['innings'] <= 2]
    unclean_dataset = unclean_dataset.groupby(
        ['match_id', 'venue', 'innings', 'batting_team', 'bowling_team']).total_runs.sum()
    unclean_dataset = unclean_dataset.reset_index()
    unclean_dataset = unclean_dataset.drop(columns=['match_id'])
    return unclean_dataset


if __name__ == "__main__":
    data = read_csv('ipl_csv2/all_matches.csv')
    clean_dataset = clean_data(data)
    clean_dataset.to_csv('clean_data.csv', index=False)
