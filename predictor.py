### Custom definitions and classes if any ###
from reader import read_encoders, read_model, read_encoders, parse_input


def predictRuns(testInput):
    prediction = 0
    
    regressor = read_model()    
    encoders = read_encoders()
    test_data = parse_input(testInput, *encoders)
    prediction = int(regressor.predict(test_data))
    return prediction

